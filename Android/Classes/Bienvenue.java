package com.example.admin123.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Bienvenue extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenue);

        final TextView text = (TextView) findViewById(R.id.textView9);

        Intent intent = this.getIntent();
        if(intent!=null){
            String textAffiche = intent.getStringExtra("WebService");
            text.setText("Bienvenue "+textAffiche);
        }

        final Button b = (Button) findViewById(R.id.jouer);

        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View vi){
                Intent intent = new Intent(getApplicationContext(), Jeu.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
