package com.example.admin123.myapplication;

/**
 * Created by Admin123 on 05/04/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class Brique {
    private BitmapDrawable image = null;
    private int x;
    private int y;
    private int hauteur;
    private int largeur;
    private int point;
    private final Context context;
    private  boolean detruit=false;

    public Brique(Context c, int x, int y,int point){
        setX(x);
        setY(y);
        setPoint(point);
        context = c;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getLargeur() {
        return largeur;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public BitmapDrawable setImage(final Context c, final int res, final int w, final int h){
        Drawable dessin = c.getResources().getDrawable(res);
        Bitmap bitmap = ((BitmapDrawable) dessin).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public void redimensioner(){
        largeur = 150;
        hauteur =70;
        setImage(setImage(context,R.mipmap.brique,largeur,hauteur));
    }

    public void draw(Canvas c){
        if(getImage()==null || isDetruit()){
            return;
        }
        c.drawBitmap(getImage().getBitmap(), x, y,null);
    }

    public BitmapDrawable getImage() {
        return image;
    }

    public void setImage(BitmapDrawable image) {
        this.image = image;
    }

    public boolean isDetruit() {
        return detruit;
    }

    public void setDetruit(boolean detruit) {
        this.detruit = detruit;
    }
}
