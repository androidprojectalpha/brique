package com.example.admin123.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created by Admin123 on 03/04/2018.
 */
public class Balle {
    private BitmapDrawable image = null;
    private int x;
    private int y;
    private int hauteur;
    private int largeur;
    private int hEcran;
    private int lEcran;
    private boolean deplacement = true;
    private static final int increment = 25;
    private int dX=increment;
    private int dY=increment;
    private final Context context;
    private int point;
    private  int vie;

    public Balle(final Context c){
        setX(500);
        setY(1350);
        setVie(3);
        context = c;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public BitmapDrawable setImage(final Context c, final int res, final int w, final int h){
        Drawable dessin = c.getResources().getDrawable(res);
        Bitmap bitmap = ((BitmapDrawable) dessin).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public boolean mouvement(){
        return deplacement;
    }

    public void deplacer(boolean depl){
        this.deplacement = depl;
    }

    public void redimensioner(int le, int he){
        lEcran = le;
        hEcran = he;
        largeur = le/25;
        hauteur = he/30;
        image = setImage(context,R.mipmap.ultra,largeur,hauteur);
    }

    public void deplacerCollision(Plateau plateau,Brique[] listeBrique){
        if(!deplacement){
            return;
        }

        int count=0;

        x+= getdX();
        y+= getdY();

        if(x+largeur+25>lEcran ){
            setdX(-increment);
        }
        if(y+hauteur>plateau.getY() && x+largeur>plateau.getX() && x<plateau.getX()+plateau.getLargeur()){
            setdY(-increment);
        }

        for(int i=0;i<listeBrique.length;i++){
            if(!listeBrique[i].isDetruit()){
                if(x+largeur>listeBrique[i].getX() && x<listeBrique[i].getX()+listeBrique[i].getLargeur()){
                    if(y<=listeBrique[i].getY()+listeBrique[i].getHauteur()){
                        setdY(increment);
                        listeBrique[i].setDetruit(true);
                        point += listeBrique[i].getPoint();
                    }
                    else if(y+hauteur>=listeBrique[i].getY() && y+hauteur<listeBrique[i].getY()+listeBrique[i].getHauteur()){
                        setdY(increment);
                        listeBrique[i].setDetruit(true);
                        point += listeBrique[i].getPoint();
                    }
                }
            }
        }

        if(x-25<0){
            setdX(increment);
        }
        if(y-25<0){
            setdY(increment);
        }
        if(y+hauteur > hEcran){
            x=500;
            y=1425;
            vie--;
            plateau.setX(400);
            plateau.setY(1500);
            deplacement =false;
        }
    }

    public void draw(Canvas c){
        if(image==null){
            return;
        }
        c.drawBitmap(image.getBitmap(), x, y,null);
    }

    public int getdX() {
        return dX;
    }

    public void setdX(int dX) {
        this.dX = dX;
    }

    public int getdY() {
        return dY;
    }

    public void setdY(int dY) {
        this.dY = dY;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }
}
