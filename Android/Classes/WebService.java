package com.example.admin123.myapplication;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.SocketTimeoutException;

/**
 * Created by Admin123 on 16/04/2018.
 */
public class WebService {
    private static String nameSpace = "http://tempuri.org/";
    private static String url = "http://192.168.43.61:69/Balle/WebService1.asmx?op=";

    public static String consumeWebService(String login, String password, String methodName){
        String reponse = "Andry";

        url+=methodName;
        SoapObject requete = new SoapObject(nameSpace, methodName);

        PropertyInfo arg1 = new PropertyInfo();
        PropertyInfo arg2 = new PropertyInfo();

        arg1.setType(String.class);
        arg1.setName("nom");
        arg1.setValue(login);

        arg2.setType(String.class);
        arg2.setName("pass");
        arg2.setValue(password);

        requete.addProperty(arg1);
        requete.addProperty(arg2);

        SoapSerializationEnvelope enveloppe = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        enveloppe.implicitTypes = true;
        enveloppe.dotNet = true;
        enveloppe.setOutputSoapObject(requete);

        HttpTransportSE http = new HttpTransportSE(url);
        http.debug = true;
        try{
            http.call(nameSpace+methodName, enveloppe);
            SoapPrimitive rep = (SoapPrimitive) enveloppe.getResponse();
            reponse = rep.toString();
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return reponse;
    }

    public static String consumeInscription(String login, String password, String argent, String methodName){
        String reponse = "Andry";

        int money = Integer.parseInt(argent);

        url+=methodName;
        SoapObject requete = new SoapObject(nameSpace, methodName);

        PropertyInfo arg1 = new PropertyInfo();
        PropertyInfo arg2 = new PropertyInfo();
        PropertyInfo arg3 = new PropertyInfo();

        arg1.setType(String.class);
        arg1.setName("nom");
        arg1.setValue(login);

        arg2.setType(String.class);
        arg2.setName("pass");
        arg2.setValue(password);

        arg3.setType(String.class);
        arg3.setName("argent");
        arg3.setValue(argent);

        requete.addProperty(arg1);
        requete.addProperty(arg2);
        requete.addProperty(arg3);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.implicitTypes =true;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(requete);

        HttpTransportSE http = new HttpTransportSE(url);
        http.debug = true;

        try{
            http.call(nameSpace+methodName, envelope);
            SoapPrimitive rep = (SoapPrimitive) envelope.getResponse();
            reponse = rep.toString();
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return reponse;
    }
}
