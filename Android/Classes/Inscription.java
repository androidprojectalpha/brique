package com.example.admin123.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Inscription extends AppCompatActivity {
    EditText login;
    EditText password;
    EditText argent;
    TextView erreur;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        Button button = (Button) findViewById(R.id.button);
        login = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);
        argent = (EditText) findViewById(R.id.editText3);

        erreur = (TextView) findViewById(R.id.error);
    }

    public void fonction2(View v){
        String loginText = login.getText().toString();
        String passText = password.getText().toString();
        String argentText = argent.getText().toString();
        MyTask2 task = new MyTask2(argentText,Inscription.this,loginText,passText);
        task.execute();
    }


    private class MyTask2 extends AsyncTask {
        String login;
        String password;
        String argent;
        String reponse;
        Inscription inscription;

        public MyTask2(String argent, Inscription inscription, String login, String password) {
            this.argent = argent;
            this.inscription = inscription;
            this.login = login;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            inscription.pb =(ProgressBar) findViewById(R.id.progressBar2);
            inscription.pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            this.reponse = WebService.consumeInscription(this.login,this.password,this.argent,"inscription");
            return null;
        }

        public void listnerIns(View v){

        }

        @Override
        protected void onPostExecute(Object o) {
            if(this.reponse.equals("OK")){
                Intent intent = new Intent(Inscription.this, Bienvenue.class);
                intent.putExtra("WebService", this.login);
                startActivity(intent);
            }
            else{
                inscription.erreur.setText("Erreur d' Inscription");
            }
            inscription.pb.setVisibility(View.GONE);
        }
    }
}
