package com.example.admin123.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final TextView text = (TextView) findViewById(R.id.text);

        Intent intent = this.getIntent();
        if(intent!=null){
            String textAffiche = intent.getStringExtra("WebService");
            text.setText("Bienvenue "+textAffiche);
        }

        final Button b = (Button) findViewById(R.id.button2);

        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View vi){
                Intent intent = new Intent(getApplicationContext(), Jeu.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
