package com.example.admin123.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Rejouer2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejouer2);

        final Button b = (Button) findViewById(R.id.rejouer2);

        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View vi){
                Intent intent = new Intent(getApplicationContext(), Jeu.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
