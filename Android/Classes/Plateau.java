package com.example.admin123.myapplication;

/**
 * Created by Admin123 on 04/04/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class Plateau {
    private BitmapDrawable image = null;
    private int x;
    private int y;
    private int hauteur;
    private int largeur;
    private boolean deplacement = true;
    private final Context context;

    public Plateau(Context c){
        setX(450);
        setY(1500);
        context = c;
    }

    public BitmapDrawable getImage() {
        return image;
    }

    public void setImage(BitmapDrawable image) {
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public BitmapDrawable setImage(final Context c, final int res, final int w, final int h){
        Drawable dessin = c.getResources().getDrawable(res);
        Bitmap bitmap = ((BitmapDrawable) dessin).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public boolean mouvement(){
        return deplacement;
    }

    public void deplacer(boolean depl){
        this.deplacement = depl;
    }

    public void redimensioner(){
        largeur = 250;
        hauteur =50;
        setImage(setImage(context,R.mipmap.raquete,largeur,hauteur));
    }

    public void draw(Canvas c){
        if(getImage()==null){
            return;
        }
        c.drawBitmap(getImage().getBitmap(), x, y,null);
    }
}
