package com.example.admin123.myapplication;

import android.content.AsyncTaskLoader;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView error;
    EditText login;
    EditText password;

    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        login = (EditText) findViewById(R.id.text);
        password = (EditText) findViewById(R.id.password);

        error = (TextView) findViewById(R.id.error);

    }

    public void fonction(View v){
        String loginText = login.getText().toString();
        String passText = password.getText().toString();
        MyTask task = new MyTask(loginText,MainActivity.this,passText);
        task.execute();
    }

    public void fonction1(View v){
        Intent intent = new Intent(MainActivity.this,Inscription.class);
        startActivity(intent);
    }

    private class MyTask extends AsyncTask{
        String login;
        String password;
        String reponse;
        MainActivity main;

        public MyTask(String login, MainActivity main, String password) {
            this.login = login;
            this.main = main;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            main.pb =(ProgressBar) findViewById(R.id.progressBar);
            main.pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            this.reponse = WebService.consumeWebService(this.login, this.password, "testLogin");
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(this.reponse.equals("OK")){
                Intent intent = new Intent(MainActivity.this, Login.class);
                intent.putExtra("WebService", this.login);
                startActivity(intent);
            }
            else{
                main.error.setText("Erreur de login et/ou password");
            }
            main.pb.setVisibility(View.GONE);
        }
    }
}
