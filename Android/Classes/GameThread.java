package com.example.admin123.myapplication;

/**
 * Created by Admin123 on 03/04/2018.
 */

import android.graphics.Canvas;

public class GameThread extends Thread{
    private final GameView view;
    private boolean run = false;

    public GameThread(GameView v){
        this.view = v;
    }

    public void setRunning(boolean run){
        this.run = run;
    }

    @Override
    public void run() {

        while(run){
            synchronized (view.getHolder()){
                view.update();
            }
            Canvas c = null;
            try{
                c = view.getHolder().lockCanvas();
                synchronized (view.getHolder()){
                    view.doDraw(c);
                }
            }
            finally {
                if(c!=null){
                    view.getHolder().unlockCanvasAndPost(c);
                }
            }
        }
    }
}
